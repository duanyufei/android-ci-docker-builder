# Mobiuspace Android build env

#########################################
# base image
FROM openjdk:8-jdk-slim

#########################################
# Author
LABEL maintainer="duanyufei@dayuwuxian.com"

#########################################
# basic toolchain
RUN apt-get update && \
  apt-get install -y unzip curl git p7zip-full && \
  apt-get clean

##########################################
## Gradle https://gradle.org/install/
#ARG gradle_version=gradle-4.4
#RUN curl --silent --show-error --location --fail --retry 3 --output /tmp/gradle.zip https://services.gradle.org/distributions/${gradle_version}-bin.zip && \
#  unzip /tmp/gradle.zip -d /opt/gradle && \
#  rm /tmp/gradle.zip
#ENV PATH="${PATH}:/opt/gradle/${gradle_version}/bin"

#########################################
# Download and install Android SDK
ARG sdk_version=sdk-tools-linux-4333796.zip
ARG android_home=/opt/android/sdk
RUN mkdir -p ${android_home} && \
    curl --silent --show-error --location --fail --retry 3 --output /tmp/${sdk_version} https://dl.google.com/android/repository/${sdk_version} && \
    unzip -q /tmp/${sdk_version} -d ${android_home} && \
    rm /tmp/${sdk_version}

#########################################
# Set environmental variables
ENV ANDROID_HOME ${android_home}
ENV ADB_INSTALL_TIMEOUT 120
ENV PATH=${ANDROID_HOME}/emulator:${ANDROID_HOME}/tools:${ANDROID_HOME}/tools/bin:${ANDROID_HOME}/platform-tools:${PATH}

#########################################
# Accept licenses
RUN yes | sdkmanager --licenses && yes | sdkmanager --update

#########################################
# Update SDK manager and install system image, platform and build tools
RUN sdkmanager \
  "tools" \
  "platform-tools" \
  "build-tools;27.0.3" \
  "build-tools;28.0.3" \
  "platforms;android-27" \
  "platforms;android-28"

#########################################
## Make User
#RUN groupadd --gid 3434 droid \
#  && useradd --uid 3434 --gid droid --shell /bin/bash --create-home droid
##  && echo 'droid ALL=NOPASSWD: ALL' >> /etc/sudoers.d/50-droid \
##  && echo 'Defaults    env_keep += "DEBIAN_FRONTEND"' >> /etc/sudoers.d/env_keep
#
#USER droid
#ENV PATH /home/droid/.local/bin:/home/droid/bin:${PATH}
#
#CMD ["/bin/bash"]
## Now commands run as user `droid`
## Switching user can confuse Docker's idea of $HOME, so we set it explicitly
#ENV HOME /home/droid


#########################################
# Copy dir
# COPY work .

#########################################
# TODO: gradle cache
# https://proandroiddev.com/speed-up-your-build-with-gradle-remote-build-cache-2ee9bfa4e18
# https://stackoverflow.com/questions/25873971/docker-cache-gradle-dependencies
# docker volume create --name gradle-cache
# docker run --rm -v gradle-cache:/home/gradle/.gradle -v "$PWD":/home/gradle/project -w /home/gradle/project gradle:4.7.0-jdk8-alpine gradle build
# ls -ltrh ./build/libs

# TODO: force gradlew comply with gradle
# TODO: check all Android sdk is installed
# TODO: gradle & kotlin version control
# TODO: shrink image
